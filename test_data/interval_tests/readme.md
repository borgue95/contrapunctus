# Content of every file

* `interval01.con`: octaves at 0 and 8
* `interval02.con`: deferred octaves at 0 and 8 
* `interval03.con`: non deferred octaves at 0 and 6. 
8
    I will only check for octaves when the two notes are different, ie, to be counted
    as deferred octaves, the four notes must be different. In the example, the 
    second _do_ is the same _do_, so they are not counted as deferred octaves. 
    
    This has a downside, when the cantus firmus repeats a note. In this example:
    
        1
        3 { la4 si do la si do }
        3 { do1 do2 }
    
    the octaves _do1_ _do_ and _do2_ _do_ are not detected. But I assume that the 
    cantus firmus will never repeat a note. 
    
    To put another example, if I consider the last example as _octaves_, this example
    will be too (and there are no octaves here):
    
        1 
        4 { do la do si }
        3 { do1 }
        
    In a near future, this will be changed and I will support both cases: the first,
    marked as _octaves_ and the second, marked as _non octaves_
         
* `interval04.con`: deferred fifths at 5 and 13
* `interval05.con`: correct deferred fifths at 2 and 8 (dissonant with cantus firmus)
* `interval06.con`: triple correct deferred fifths
* `interval07.con`: correct deferred fifths at 4 and 12 (weak tempo and contrary movement)
* `interval08.con`: deferred fifths at 4 and 12 (weak tempo but direct movement)
* `interval09.con`: more than three thirds in a row
* `interval10.con`: more than three sixths in a row





