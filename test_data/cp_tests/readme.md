# Content of every file

* `cp01.con`: correct. Contrary movement
* `cp02.con`: correct. Contrary movement
* `cp03.con`: incorrect. Direct movement with 2 voices
* `cp04.con`: correct. Direct movement with 3 voices and one goes in joint degrees
* `cp05.con`: incorrect. Extreme voices and soprano does not go in joint degrees. 
    Violates other rules, but I only check for perfect consonants in this test.
* `cp06.con`: correct. Extreme voices and soprano does go in joint degrees