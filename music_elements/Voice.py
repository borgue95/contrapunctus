import copy
from music_elements.Note import Note


class Voice:
    def __init__(self, min_value):
        self.__minValue = min_value
        self.notes = []
        self.real_notes = []  # type: list[Note]
        self.is_cantus_firmus = False
        self.note_count = 0

    def add_note(self, note):
        note.tempo_mark = self.note_count
        self.note_count += 1

        self.notes.append(note)
        self.real_notes.append(note)

        for i in range(1, int(self.__minValue / note.duration)):
            new_note = copy.deepcopy(note)  # type: Note
            new_note.is_articulated = False
            new_note.tempo_mark = self.note_count
            self.note_count += 1
            self.notes.append(new_note)

    def get_note_on_tempo_mark(self, tempo_mark) -> Note:
        note = list(filter(lambda x: x.tempo_mark == tempo_mark, self.notes))
        assert note is not None
        return note[0]

    def set_voice_to_be_cantus_firmus(self):
        self.is_cantus_firmus = True
        for note in self.notes:
            note.is_cantus_firmus = True
        for note in self.real_notes:
            note.is_cantus_firmus = True
