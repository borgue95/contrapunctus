from music_elements.Note import Note


class Interval:

    MOV_CONTRARI = "contrari"
    MOV_OBLIC = "oblic"
    MOV_DIRECTE = "directe"

    def __init__(self, two_notes, tempo_mark):
        self.note1 = two_notes[0]  # type: Note
        self.note2 = two_notes[1]  # type: Note
        self.tempo_mark = tempo_mark
        self.last_tempo_mark = -1
        self.redundant = self.__is_redundant()
        self.real_interval = self.__compute_interval()
        self.relative_interval = self.__compute_relative_interval()

    def __is_redundant(self):
        return (not self.note1.is_articulated and
                not self.note2.is_articulated and
                not self.note1.is_tied and
                not self.note2.is_tied) or (
                self.note1.is_rest or
                self.note2.is_rest)

    def __compute_interval(self):
        return abs(self.note1.absolute_note - self.note2.absolute_note)

    def __compute_relative_interval(self):
        if self.real_interval == 0:
            return 0  # Unison

        tmp = self.real_interval % 12
        if tmp == 0:
            return 12

        return tmp

    def get_movement_with(self, other):
        if self.note1.absolute_note > other.note1.absolute_note:
            # baix decreix
            if self.note2.absolute_note > other.note2.absolute_note:
                return self.MOV_DIRECTE
            elif self.note2.absolute_note < other.note2.absolute_note:
                return self.MOV_CONTRARI
            else:
                return self.MOV_OBLIC
        elif self.note1.absolute_note < other.note1.absolute_note:
            # baix creix
            if self.note2.absolute_note < other.note2.absolute_note:
                return self.MOV_DIRECTE
            elif self.note2.absolute_note > other.note2.absolute_note:
                return self.MOV_CONTRARI
            else:
                return self.MOV_OBLIC
        else:
            return self.MOV_OBLIC

    def is_unison(self):
        return self.relative_interval == 0

    def is_second(self):
        return self.relative_interval == 1 or self.relative_interval == 2

    def is_joint_degree(self):
        return self.is_second()

    def is_third(self):
        return self.relative_interval == 3 or self.relative_interval == 4

    def is_fourth(self):
        return self.relative_interval == 5

    def is_sixth(self):
        return self.relative_interval == 8 or self.relative_interval == 9

    def is_octave(self):
        return self.relative_interval == 12

    def is_consonant(self):
        # fourth depends...
        return self.relative_interval in [0, 3, 4, 7, 8, 9, 12]

    def is_dissonant(self):
        # fourth depends...
        return self.relative_interval in [1, 2, 6, 10, 11]

    def is_perfectly_consonant(self):
        return self.relative_interval == 0 or self.relative_interval == 7 or self.relative_interval == 12

    @staticmethod
    def get_relative_interval(note1, note2):
        """
        :type note1: Note
        :type note2: Note
        :return: int
        """
        absolute_interval = abs(note1.absolute_note - note2.absolute_note)

        if absolute_interval == 0:
            return 0

        relative_interval = absolute_interval % 12
        if relative_interval == 0:
            return 12
        return relative_interval


