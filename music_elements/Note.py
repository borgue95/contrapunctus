import re


class Note:
    note_translation = {"do": 1, "sid": 1,
                        "dod": 2, "reb": 2,
                        "re": 3,
                        "red": 4, "mib": 4,
                        "mi": 5, "fab": 5,
                        "fa": 6, "mid": 6,
                        "fad": 7, "solb": 7,
                        "sol": 8,
                        "sold": 9, "lab": 9,
                        "la": 10,
                        "lad": 11, "sib": 11,
                        "si": 12, "dob": 12}

    def __init__(self, literal, height=-1, duration=0, is_cantus_firmus=False, voice_number=0):
        self.is_rest = False
        self.literal = literal  # ex: do, lab'4 re
        self.height = height

        self.note = ""
        self.clean_note = ""
        self.__extract_note()  # ex: note: do lab re   and   clean_note: do la re

        self.duration = -1
        self.__extract_duration(duration)

        self.modifier = ""
        self.modifier_quantity = 0
        self.__extract_modifiers()

        self.absolute_note = -1
        self.__compute_absolute_note()  # a number: 0 == Do0, 1 == Dod0 & Reb0, etc

        self.is_articulated = True
        self.is_cantus_firmus = is_cantus_firmus
        self.voice_number = voice_number
        self.is_tied = False
        self.tempo_mark = 0

    def __extract_note(self):
        note_regex = '^(x|do|re|mi|fa|sol|la|si)(d|b)*'
        self.note = re.search(note_regex, self.literal).group(0)
        self.clean_note = re.search(note_regex, self.literal).group(1)

        if self.clean_note == 'x':
            self.is_rest = True

    def __extract_duration(self, duration):
        # TODO when supporting half duration, refactor this
        if duration == 0:
            duration_regex = '(1|2|4|8)?$'
            duration_str = re.search(duration_regex, self.literal).group(0)
            if duration_str != '':
                self.duration = int(duration_str)
            else:
                self.duration = 0
        else:
            self.duration = duration

    def __extract_modifiers(self):
        if "'" in self.literal:
            self.modifier = "'"
            self.modifier_quantity = self.literal.count("'")
        elif "," in self.literal:
            self.modifier = ","
            self.modifier_quantity = self.literal.count(",")
        else:
            self.modifier = ""
            self.modifier_quantity = 0

    def __compute_absolute_note(self):
        if not self.is_rest:
            self.absolute_note = self.note_translation[self.note] + 12 * (self.height - 1)
        else:
            self.absolute_note = -1

    def set_height(self, height):
        self.height = height
        self.__compute_absolute_note()

    def get_specie(self):
        if self.duration == 1:
            return 1
        if self.duration == 2:
            return 2
        if self.is_tied:
            return 4
        if self.duration == 4 or self.duration == 8:
            return 3

    def __eq__(self, other):
        if isinstance(other, Note):
            # compare
            return \
                self.literal == other.literal and \
                self.height == other.height and \
                self.duration == other.duration and \
                self.is_articulated == other.is_articulated and \
                self.voice_number == other.voice_number
        return False

    def __str__(self):
        return str(self.note) + \
               str(self.modifier * self.modifier_quantity) + \
               str(self.duration) + \
               "(" + str(self.height) + ")"
