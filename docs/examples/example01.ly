\version "2.18.2"
\language "catalan"
\include "lilypond-book-preamble.ly"

\header {
  % Remove default LilyPond tagline
  tagline = ##f
}


global = {
  \key do \major
  \time 4/4
  %\tempo 4=100
}

sopranoVoice = \relative do' {
  \global
  % Music follows here.
  do4 re mi fa sol do fa,2
}

verse = \lyricmode {
  % Lyrics follow here.
  
}

right = \relative do'' {
  \global
  % Music follows here.
  r4 sol fa mi fad2 la re,4 do mib4 fa
}

left = \relative do {
  \global
  % Music follows here.
  do1 re sol
}

sopranoVoicePart = \new Staff { \sopranoVoice }
\addlyrics { \verse }

pianoPart = \new PianoStaff
<<
  \new Staff = "right" \right
  \new Staff = "left" { \clef bass \left }
>>

%\score {
%  <<
%    \sopranoVoicePart
%  >>
%  \layout { }
%}

\score {
  <<
    \pianoPart
  >>
  \layout { }
}
