from music_elements.Interval import Interval
from music_elements.Voice import Voice
from tests.Error import Error


class TestIntervals:
    def __init__(self, interval_voice, cf_voice):
        self.interval_voice = interval_voice  # type: list[Interval]
        self.cantus_firmus = cf_voice  # type: Voice
        self.errors = []  # type: list[Error]

    def run_tests(self):
        self.__check_octaves()
        self.__check_deferred_octaves()
        self.__check_fifths()
        self.__check_deferred_fifths()
        self.__check_thirds()
        self.__check_sixths()

    def print_errors(self):
        for error in self.errors:
            print(str(error))

    def __check_interval(self, interval, name):
        for i in range(len(self.interval_voice) - 1):
            interval1 = self.interval_voice[i]
            interval2 = self.interval_voice[i + 1]
            if interval1.relative_interval == interval and interval2.relative_interval == interval:
                if self.__is_consonant_with_cantus_firmus(interval1, interval2):
                    self.errors.append(Error(Error.ERROR,
                                             "Found " + name,
                                             [interval1.tempo_mark, interval2.tempo_mark]))

    def __check_deferred_interval(self, interval, name):
        last_interval = None
        last_interval_index = len(self.interval_voice)

        # find the first interval to compare with
        for i in range(len(self.interval_voice)):
            if self.interval_voice[i].relative_interval == interval and last_interval is None:
                last_interval = self.interval_voice[i]
                last_interval_index = i
                break

        for i in range(last_interval_index + 1, len(self.interval_voice)):
            is_ok = True

            # 1. Must be the interval we are checking
            # 2. Must be at one or more distance (deferred intervals)
            is_ok &= self.interval_voice[i].relative_interval == interval
            is_ok &= abs(i - last_interval_index) > 1

            # Diagram:
            # 3. All notes must be different
            # distance <= 8?
            # - no --> OK
            # - yes:
            #     contrary movement?
            #     - no
            #         consonants w/ cantus firmus?
            #         - no --> OK
            #         - yes --> ERR
            #     - yes
            #         weak tempo?
            #         - no --> ERR
            #         - yes --> OK
            #

            if is_ok:
                if self.__all_notes_are_different(last_interval, self.interval_voice[i]):
                    distance = abs(last_interval.last_tempo_mark - self.interval_voice[i].tempo_mark)
                    if distance <= 8:
                        if self.__is_contrary_movement(last_interval, self.interval_voice[i]):
                            if not self.__is_in_weak_tempo(self.interval_voice[i]):
                                # ERR
                                self.errors.append(Error(Error.ERROR,
                                                         "Found deferred " + name,
                                                         [last_interval.tempo_mark, self.interval_voice[i].tempo_mark]))
                        else:
                            if self.__is_consonant_with_cantus_firmus(last_interval, self.interval_voice[i]):
                                # ERR
                                self.errors.append(Error(Error.ERROR,
                                                         "Found deferred " + name,
                                                         [last_interval.tempo_mark, self.interval_voice[i].tempo_mark]))
                last_interval = self.interval_voice[i]
                last_interval_index = i

    @staticmethod
    def __is_contrary_movement(interval1, interval2):
        movement = interval1.get_movement_with(interval2)
        return movement == Interval.MOV_CONTRARI or movement == Interval.MOV_OBLIC

    @staticmethod
    def __is_in_weak_tempo(interval):
        # TODO I don't know which tempo is weak (2, 3 and 4?, 2 and 4? depend on specie?)
        return interval.tempo_mark % 8 >= 2

    def __is_consonant_with_cantus_firmus(self, interval1, interval2):
        if interval1.note1.is_cantus_firmus or interval1.note2.is_cantus_firmus:
            return True  # There is a mistake for sure
        else:
            # check if one of the four notes is dissonant with the cantus firmus voice
            cantus_firmus_note1 = self.cantus_firmus.notes[interval1.tempo_mark]
            cantus_firmus_note2 = self.cantus_firmus.notes[interval2.tempo_mark]
            dissonant_intervals = [1, 2, 5, 6, 10, 11]
            if Interval.get_relative_interval(interval1.note1, cantus_firmus_note1) in dissonant_intervals or \
                    Interval.get_relative_interval(interval1.note2, cantus_firmus_note1) in dissonant_intervals or \
                    Interval.get_relative_interval(interval2.note1, cantus_firmus_note2) in dissonant_intervals or \
                    Interval.get_relative_interval(interval2.note2, cantus_firmus_note2) in dissonant_intervals:
                return False  # No error
            else:
                return True  # Error

    @staticmethod
    def __all_notes_are_different(interval1, interval2):
        return \
            interval1.note1.absolute_note != interval1.note2.absolute_note and \
            interval1.note1.absolute_note != interval2.note1.absolute_note and \
            interval1.note1.absolute_note != interval2.note2.absolute_note and \
            interval1.note2.absolute_note != interval2.note1.absolute_note and \
            interval1.note2.absolute_note != interval2.note2.absolute_note and \
            interval2.note1.absolute_note != interval2.note2.absolute_note

    def __check_octaves(self):
        self.__check_interval(12, "octaves")

    def __check_deferred_octaves(self):
        self.__check_deferred_interval(12, "octaves")

    def __check_fifths(self):
        self.__check_interval(7, "fifths")

    def __check_deferred_fifths(self):
        self.__check_deferred_interval(7, "fifths")

    def __check_thirds(self):
        thirds_in_a_row = 0

        for interval in self.interval_voice:
            if interval.is_third():
                thirds_in_a_row += 1
            else:
                thirds_in_a_row = 0

            if thirds_in_a_row > 3:
                self.errors.append(Error(Error.ERROR,
                                         "Found more than 3 thirds in a row",
                                         [interval.tempo_mark]))

    def __check_sixths(self):
        sixths_in_a_row = 0

        for interval in self.interval_voice:
            if interval.is_sixth():
                sixths_in_a_row += 1
            else:
                sixths_in_a_row = 0

            if sixths_in_a_row > 3:
                self.errors.append(Error(Error.ERROR,
                                         "Found more than 3 sixths in a row",
                                         [interval.tempo_mark]))

