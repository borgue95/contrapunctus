# Regles del contrapunt

## Regles genèriques

### Regles melòdiques

* Només salts de segona, tercera, quarta i quinta justa i sisena

* No estan permesos salts disminuïts o augmentats

### Regles intervàliques

* No es permeten salts de quina i octava successius


## 1ra espècie

* S'articula una nota per nota del contrapunt.

## 2na espècie

* S'articulen dues notes per nota del contrapunt.

* La nota en temps fort ha de ser consonant.

* La nota en temps semifort, pot ser dissonant.

## 3ra espècie

* S'articulen quatre notes per nota del contrapunt.

* Es permeten corxeres en els temps dèbils, sempre i quan s'hi arribi i se'n surti per graus conjunts.

