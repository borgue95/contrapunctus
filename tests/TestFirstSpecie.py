from music_elements.Voice import Voice
from music_elements.Interval import Interval
from music_elements.Note import Note
from tests.Error import Error


class TestFirstSpecie:
    # Apply the rules
    # per totes les rodones que es trobin.

    def __init__(self, voice, cf_voice):
        self.voice = voice  # type: Voice
        self.cantus_firmus = cf_voice  # type: Voice
        self.errors = []  # type: list[Error]

    def run_tests(self):
        self.__check_consonant_notes()
        self.__check_beginning()
        self.__check_end()
        self.__check_interval_beginning()

    def print_errors(self):
        for err in self.errors:
            print(err)

    def __check_consonant_notes(self):
        for note in self.voice.real_notes:
            if note.get_specie() == 1:
                cf_note = self.cantus_firmus.get_note_on_tempo_mark(note.tempo_mark)  # type: Note
                interval = Interval([note, cf_note], note.tempo_mark)
                if interval.is_dissonant() or interval.is_fourth():
                    self.errors.append(Error(Error.ERROR,
                                             "Found dissonant interval in first specie",
                                             [note.tempo_mark]))

    def __check_beginning(self):
        if self.voice.real_notes[0].get_specie() == 1:
            interval = Interval([self.voice.real_notes[0], self.cantus_firmus.real_notes[0]], 0)
            if not interval.is_perfectly_consonant():
                self.errors.append(Error(Error.ERROR,
                                         "Beginning of contrapunctus is not perfectly consonant",
                                         [0]))

    def __check_end(self):
        last_note = self.voice.real_notes[len(self.voice.real_notes) - 1]

        # assumes a cantus firmus of whole notes
        if last_note.get_specie() == 1:
            interval = Interval([last_note, self.cantus_firmus.get_note_on_tempo_mark(last_note.tempo_mark)],
                                last_note.tempo_mark)
            if not interval.is_perfectly_consonant():
                self.errors.append(Error(Error.ERROR,
                                         "End of contrapunctus is not perfectly consonant",
                                         [last_note.tempo_mark]))
        else:
            self.errors.append(Error(Error.WARNING,
                                     "The last note should be a whole note",
                                     [last_note.tempo_mark]))

    def __check_interval_beginning(self):
        if self.voice.real_notes[0].get_specie() == 1:
            note = self.voice.real_notes[0]
            cf_note = self.cantus_firmus.real_notes[0]
            if note.absolute_note < cf_note.absolute_note:  # contrapunctus below cantus firmus
                # only octaves and unison
                interval = Interval([note, cf_note], 0)
                # !(a || b) == !a && !b
                if not interval.is_octave() and not interval.is_unison():
                    self.errors.append(Error(Error.ERROR,
                                             "Beginning of contrapunctus must be octave or unison",
                                             [0]))
