from music_elements.Voice import Voice
from music_elements.Interval import Interval
from tests.Error import Error


class TestSecondSpecie:
    # dissonancies en temps dèbil han d'arribar i marxar per graus conjunts
    # CF i CP articulats => consonància perfecte

    def __init__(self, voice, cf_voice):
        self.voice = voice  # type: Voice
        self.cantus_firmus = cf_voice  # type: Voice
        self.errors = []  # type: list[Error]

    def run_tests(self):
        self.__check_consonant_with_cantus_firmus()
        self.__check_movement_in_dissonant_notes()

    def print_errors(self):
        for err in self.errors:
            print(err)

    def __check_consonant_with_cantus_firmus(self):
        for note in self.voice.real_notes:
            if note.get_specie() == 2:
                cf_note = self.cantus_firmus.get_note_on_tempo_mark(note.tempo_mark)
                if cf_note.is_articulated:
                    interval = Interval([note, cf_note], note.tempo_mark)
                    if interval.is_dissonant() or interval.is_fourth():
                        self.errors.append(Error(Error.ERROR,
                                                 "Found dissonant interval when articulating cantus firmus in second "
                                                 "specie",
                                                 [note.tempo_mark]))

    def __check_movement_in_dissonant_notes(self):
        for i in range(1, len(self.voice.real_notes) - 1):
            note = self.voice.real_notes[i]
            if note.get_specie() == 2:
                cf_note = self.cantus_firmus.get_note_on_tempo_mark(note.tempo_mark)
                interval = Interval([note, cf_note], note.tempo_mark)
                if (interval.is_dissonant() or interval.is_fourth()) and not cf_note.is_articulated:
                    # before and after: joint degrees
                    note_before = self.voice.real_notes[i-1]
                    note_after = self.voice.real_notes[i+1]

                    interval_before = Interval([note_before, note], note.tempo_mark)
                    interval_after = Interval([note, note_after], note.tempo_mark)

                    if not interval_before.is_joint_degree():
                        self.errors.append(Error(Error.ERROR,
                                                 "Found non joint degree before dissonant note in second specie",
                                                 [note_before.tempo_mark, note.tempo_mark]))
                    if not interval_after.is_joint_degree():
                        self.errors.append(Error(Error.ERROR,
                                                 "Found non joint degree after dissonant note in second specie",
                                                 [note.tempo_mark, note_after.tempo_mark]))
