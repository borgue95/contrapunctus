from music_elements.Voice import Voice
from music_elements.Interval import Interval
from tests.Error import Error


class TestFourthSpecie:

    def __init__(self, voice, cf_voice):
        self.voice = voice  # type: Voice
        self.cantus_firmus = cf_voice  # type: Voice
        self.errors = []  # type: list[Error]

        self.everything_is_ok = True  # prevent error snowballing

    def run_tests(self):
        self.__check_tempo_mark_of_tied_note()
        if self.everything_is_ok:
            self.__check_resolution()

    def print_errors(self):
        for err in self.errors:
            print(err)

    def __check_tempo_mark_of_tied_note(self):
        for note in self.voice.real_notes:
            if note.is_tied and note.tempo_mark % 8 != 0:
                self.errors.append(Error(Error.ERROR,
                                         "Found tied note on non hard beat",
                                         [note.tempo_mark]))
                self.everything_is_ok = False

    def __check_resolution(self):
        for note in self.voice.real_notes:
            if note.is_tied:
                cf_note = self.cantus_firmus.get_note_on_tempo_mark(note.tempo_mark)
                interval = Interval([note, cf_note], -1)
                if interval.is_dissonant() or interval.is_fourth():  # TODO 4ta segons si el cf està a baix o no
                    # needs to be resolved at third tempo (5th tempo mark)
                    resolution = self.voice.get_note_on_tempo_mark(note.tempo_mark + 4)
                    if resolution is None:
                        self.errors.append(Error(Error.ERROR,
                                                 "Resolution of dissonance in tied note not found",
                                                 [note.tempo_mark]))

                    # 2 conditions: resolution must be consonant w/ cf and resolution must be in a second w/ note
                    interval = Interval([resolution, self.cantus_firmus.get_note_on_tempo_mark(resolution.tempo_mark)], -1)
                    if not interval.is_consonant():
                        self.errors.append(Error(Error.ERROR,
                                                 "Found non consonant resolution of tied dissonance",
                                                 [resolution.tempo_mark]))

                    interval = Interval([resolution, note], -1)
                    if not interval.is_second():
                        self.errors.append(Error(Error.ERROR,
                                                 "Not found resolution of note at distance of second",
                                                 [note.tempo_mark, resolution.tempo_mark]))
                else:
                    # is consonant. OK! but third tempo must be consonant as well
                    third_tempo = self.voice.get_note_on_tempo_mark(note.tempo_mark + 4)
                    interval = Interval([third_tempo, self.cantus_firmus.get_note_on_tempo_mark(third_tempo.tempo_mark)], -1)
                    if not interval.is_consonant():
                        self.errors.append(Error(Error.ERROR,
                                                 "Third tempo, in fourth specie, must be consonant with cantus firmus",
                                                 [third_tempo.tempo_mark]))

