from utils import TermColors


class Error:
    ERROR = 1
    WARNING = 2
    INFO = 3

    def __init__(self, err_type, message, location, voice_numbers=[]):
        # Error, Warning, Info
        self.type = err_type  # type: int

        # String message - human readable
        self.message = message  # type: str

        # Tempo marks where the error is located
        self.location = location  # type: list[int]

        # Voices affected (optional)
        self.voice_numbers = voice_numbers

    def __str__(self):
        err = ""

        # type
        if self.type == self.ERROR:
            err += TermColors.BOL + TermColors.RED + "ERROR    "
        elif self.type == self.WARNING:
            err += TermColors.BOL + TermColors.YEL + "WARNING  "
        elif self.type == self.INFO:
            err += TermColors.BOL + TermColors.CYA + "INFO     "

        # message
        err += TermColors.REM
        err += self.message
        err += TermColors.BOL
        err += " at "
        err += TermColors.REM

        # location
        err += Error.pretty_location(self.location[0])
        for i in range(1, len(self.location) - 1):
            err += ", "
            err += Error.pretty_location(self.location[i])
        if len(self.location) > 1:
            err += " and "
            err += Error.pretty_location(self.location[len(self.location) - 1])

        # voice affected
        if len(self.voice_numbers) > 0:
            err += " in voice/s "
            err += self.voice_numbers[0]
            for i in range(1, len(self.location) - 1):
                err += ", "
                err += self.voice_numbers[i]
            if len(self.voice_numbers) > 1:
                err += " and "
                err += self.voice_numbers[len(self.voice_numbers) - 1]

        return err

    def __eq__(self, other):
        if isinstance(other, Error):
            # compare
            return \
                self.type == other.type and \
                self.location == other.location and \
                self.message == other.message
        return False

    @staticmethod
    def pretty_location(location):
        measure = int(location / 8) + 1
        tempo = int((location % 8) / 2) + 1
        exact_point = location % 8 + 1

        return "measure " + str(measure) + ", tempo " + str(tempo) + " (" + str(exact_point) + ")"

