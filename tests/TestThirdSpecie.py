from music_elements.Voice import Voice
from music_elements.Interval import Interval
from tests.Error import Error


class TestThirdSpecie:
    # resolució correcte (immediata o diferida) de dissonàncies
    # correcció en corxeres

    def __init__(self, voice, cf_voice):
        self.voice = voice  # type: Voice
        self.cantus_firmus = cf_voice  # type: Voice
        self.errors = []  # type: list[Error]

        # temp variables
        self.__deferred_left = False
        self.__deferred_right = False

    def run_tests(self):
        self.__check_resolution_of_dissonances()
        self.__check_eights()

    def print_errors(self):
        for error in self.errors:
            print(error)

    def __check_resolution_of_dissonances(self):
        for i, note in enumerate(self.voice.real_notes):
            if note.get_specie() == 3:
                cf_note = self.cantus_firmus.get_note_on_tempo_mark(note.tempo_mark)
                interval = Interval([note, cf_note], note.tempo_mark)
                if interval.is_dissonant() or interval.is_fourth():
                    # pass the tests here
                    is_escapada = self.__check_for_escapada(i, note)
                    if not is_escapada:
                        self.__look_left(i, note)
                        self.__look_right(i, note)

                if self.__deferred_left:
                    in_between_note = self.voice.real_notes[i - 1]
                    interval = Interval([
                        in_between_note, self.cantus_firmus.get_note_on_tempo_mark(in_between_note.tempo_mark)
                    ], -1)
                    if interval.is_dissonant() or interval.is_fourth():
                        interval2 = Interval([in_between_note, self.voice.real_notes[i - 2]], -1)
                        if not interval2.is_second():
                            self.errors.append(Error(Error.ERROR,
                                                     "Found incorrect resolution of deferred dissonance in third specie",
                                                     [in_between_note.tempo_mark,
                                                      self.voice.real_notes[i - 2].tempo_mark]))

                if self.__deferred_right:
                    in_between_note = self.voice.real_notes[i + 1]
                    interval = Interval([
                        in_between_note, self.cantus_firmus.get_note_on_tempo_mark(in_between_note.tempo_mark)
                    ], -1)
                    if interval.is_dissonant() or interval.is_fourth():
                        interval2 = Interval([in_between_note, self.voice.real_notes[i + 2]], -1)
                        if not interval2.is_second():
                            self.errors.append(Error(Error.ERROR,
                                                     "Found incorrect resolution of deferred dissonance in third specie",
                                                     [in_between_note.tempo_mark,
                                                      self.voice.real_notes[i + 2].tempo_mark]))

                self.__deferred_right = False
                self.__deferred_left = False

    def __check_for_escapada(self, i, note):
        # 2n asc , diss , 3ra desc
        if i - 1 < 0 or i + 1 >= len(self.voice.real_notes):
            return False

        note_before = self.voice.real_notes[i - 1]
        note_after = self.voice.real_notes[i + 1]

        interval_before = Interval([note_before, note], -1)
        interval_after = Interval([note, note_after], -1)

        return \
            interval_before.is_second() and \
            note_before.absolute_note < note.absolute_note and \
            interval_after.is_third() and \
            note.absolute_note > note_after.absolute_note

    def __look_left(self, i, note, look_up_even_further=True):
        if i - 1 < 0:
            return False

        interval = Interval([note, self.voice.real_notes[i - 1]], -1)
        if interval.is_second():
            interval2 = Interval([
                self.cantus_firmus.get_note_on_tempo_mark(self.voice.real_notes[i - 1].tempo_mark),
                self.voice.real_notes[i - 1]
            ], -1)
            if interval2.is_consonant():
                return True
            else:
                self.errors.append(Error(Error.ERROR,
                                         "Found dissonant note before dissonance in joint degree",
                                         [note.tempo_mark, self.voice.real_notes[i - 1].tempo_mark]))
                return False
        else:
            # look up one more note back
            if look_up_even_further:
                correct = self.__look_left(i - 1, note, False)
                if correct:
                    self.__deferred_left = True
                return correct
            else:
                self.errors.append(Error(Error.ERROR,
                                         "Not found consonance before deferred resolution of dissonance",
                                         [note.tempo_mark, self.voice.real_notes[i - 1].tempo_mark]))
                return False

    def __look_right(self, i, note, look_up_even_further=True):
        if i + 1 >= len(self.voice.real_notes):
            return False

        interval = Interval([note, self.voice.real_notes[i + 1]], -1)
        if interval.is_second():
            interval2 = Interval([
                self.cantus_firmus.get_note_on_tempo_mark(self.voice.real_notes[i + 1].tempo_mark),
                self.voice.real_notes[i + 1]
            ], -1)
            if interval2.is_consonant():
                return True
            else:
                self.errors.append(Error(Error.ERROR,
                                         "Found dissonant note after dissonance in joint degree",
                                         [note.tempo_mark, self.voice.real_notes[i + 1].tempo_mark]))
                return False
        else:
            # look up one more note back
            if look_up_even_further:
                correct = self.__look_right(i + 1, note, False)
                if correct:
                    self.__deferred_right = True
                return correct
            else:
                self.errors.append(Error(Error.ERROR,
                                         "Not found consonance after deferred resolution of dissonance",
                                         [note.tempo_mark, self.voice.real_notes[i + 1].tempo_mark]))
                return False

    def __check_eights(self):
        self.__check_number()
        self.__check_tempos()
        self.__check_resolutions()  # TODO best name: check for "this" eights "that"
        self.__check_intervals()

    def __check_number(self):
        i = 0
        while i < len(self.voice.real_notes) - 1:
            note = self.voice.real_notes[i]
            if note.duration == 8:
                if self.voice.real_notes[i + 1].duration != 8:
                    # ERR
                    self.errors.append(Error(Error.ERROR,
                                             "Found invalid number of eights",
                                             [note.tempo_mark]))
                i += 2  # I will not look for the next
            else:
                i += 1

    def __check_tempos(self):
        for note in self.voice.real_notes:
            if note.duration == 8:
                tempo = note.tempo_mark % 8
                if tempo not in [2, 3, 6, 7]:
                    self.errors.append(Error(Error.ERROR,
                                             "Found eighths in non soft tempos",
                                             [note.tempo_mark]))

    def __check_resolutions(self):
        i = 1
        while i < len(self.voice.real_notes) - 2:
            note = self.voice.real_notes[i]
            if note.duration == 8:
                # before: 2nd
                interval = Interval([self.voice.real_notes[i - 1], note], -1)
                if not interval.is_second():
                    self.errors.append(Error(Error.ERROR,
                                             "Found non second before eighth",
                                             [note.tempo_mark]))

                i += 1
                note = self.voice.real_notes[i]
                if note.duration == 8:  # Must be, but I put this to prevent error snowballing
                    interval = Interval([note, self.voice.real_notes[i + 1]], -1)
                    if not interval.is_second():
                        self.errors.append(Error(Error.ERROR,
                                                 "Found non second after eighth",
                                                 [note.tempo_mark]))

            i += 1

    def __check_intervals(self):
        i = 0
        while i < len(self.voice.real_notes) - 2:
            note1 = self.voice.real_notes[i]
            i += 1
            note2 = self.voice.real_notes[i]

            if note1.duration == 8 and note2.duration == 8:
                interval = Interval([note1, note2], -1)
                if not interval.is_second() and not interval.is_third():
                    self.errors.append(Error(Error.ERROR,
                                             "Found an interval different of second or third between two eighths",
                                             [note1.tempo_mark, note2.tempo_mark]))

                if interval.is_third():
                    # The next note must be in a second (in between note1 and note2)
                    note3 = self.voice.real_notes[i + 1]
                    interval1 = Interval([note1, note3], -1)
                    interval2 = Interval([note2, note3], -1)

                    if not interval1.is_second() or not interval2.is_second():
                        self.errors.append(Error(Error.ERROR,
                                                 "Not found 'cambiata' note in between two eighths",
                                                 [note3.tempo_mark]))

            else:
                # The error must be detected in other tests, so I pass
                # Prevent error snowballing
                pass
