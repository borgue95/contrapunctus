from music_elements.Interval import Interval
from music_elements.Voice import Voice
from tests.Error import Error


class TestCP:
    # Test the most important rule of Contrapunctus:
    # A dues veus, s'ha d'arribar a una consonància perfecte per moviment contrari
    # A més veus, s'hi pot arribar per moviment directe si alguna de les veus hi arriba per graus conjunts
    # Si les dues veus són extremes, només serà vàlid si la superior va per graus conjunts

    def __init__(self, interval_voice, cf_voice, num_voices):
        self.interval_voice = interval_voice  # type: list[Interval]
        self.cantus_firmus = cf_voice  # type: Voice
        self.num_voices = num_voices
        self.errors = []  # type: list[Error]

    def run_tests(self):
        self.__check_cp()

    def print_errors(self):
        for err in self.errors:
            print(err)

    def __check_cp(self):
        # Per cada CP que trobem, hem de comprovar com s'hi arriba

        for i in range(1, len(self.interval_voice)):
            possible_error = False  # to reduce indentation a little bit
            interval = self.interval_voice[i]
            if interval.is_perfectly_consonant():
                movement = self.interval_voice[i-1].get_movement_with(interval)
                if movement == Interval.MOV_DIRECTE:
                    if self.num_voices == 2:
                        self.errors.append(Error(Error.ERROR,
                                                 "Found CP reached by direct movement",
                                                 [interval.tempo_mark]))
                    else:
                        possible_error = True

            if possible_error:
                # alguna de les veus va per graus conjunts
                if not self.__some_voice_by_joint_degree(self.interval_voice[i-1], interval):
                    self.errors.append(Error(Error.ERROR,
                                             "Found CP reached by direct movement",
                                             [interval.tempo_mark]))
                if self.__extreme_voices() and not self.__soprano_by_joint_degree(self.interval_voice[i-1], interval):
                    self.errors.append(Error(Error.ERROR,
                                             "Found CP reached by direct movement",
                                             [interval.tempo_mark]))

    def __some_voice_by_joint_degree(self, interval1, interval2):
        superior = Interval.get_relative_interval(interval1.note1, interval2.note1)
        inferior = Interval.get_relative_interval(interval1.note2, interval2.note2)

        joint_degrees = [1, 2]
        return superior in joint_degrees or inferior in joint_degrees

    def __extreme_voices(self):
        # pick the first interval
        interval = self.interval_voice[0]

        # pick the high note and the low note
        high = interval.note1
        low = interval.note2

        # extreme = high == 0 and low == num_voices - 1
        return high.voice_number == 0 and low.voice_number == self.num_voices - 1

    def __soprano_by_joint_degree(self, interval1, interval2):
        superior = Interval.get_relative_interval(interval1.note1, interval2.note1)

        joint_degrees = [1, 2]
        return superior in joint_degrees

