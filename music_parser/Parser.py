import re
from music_elements.Note import Note
from music_elements.Voice import Voice


# TODO fer-ne una classe amb una llista d'errors i tal....

# TODO if I make better regular expressions here and in Note.py, I will can support rest as 'r' not 'x'.
# Now, re is picked up as rest...


def it_is_a_note(elem):
    regex = '^(x|do|re|mi|fa|sol|la|si)(d|b)*(,*|\'*)(1|2|4|8)?$'
    return re.match(regex, elem) is not None


def is_what_i_need(elem, expected):
    expected_that = False
    for i in expected:
        if i == 'number':
            expected_that |= elem.isnumeric()
        elif i == '~':
            expected_that |= elem == '~'
        elif i == '{':
            expected_that |= elem == '{'
        elif i == '}':
            expected_that |= elem == '}'
        elif i == 'note':
            expected_that |= it_is_a_note(elem)

    return expected_that


def print_error_msg(line, expected, found):
    string = "Syntax error at line " + str(line) + ". I expected "

    for i in range(len(expected) - 1):
        string += expected[i] + " or "
    string += expected[len(expected) - 1]

    string += ". I found that: " + found

    print(string)


def parse_individual_notes(last, current):
    current.set_height(last.height)

    if current.duration == 0:
        current.duration = last.duration

    if current.is_rest:
        return current

    if last.is_rest:
        # only at beginning when the first note is a rest
        return current

    last_literal_clean = last.clean_note
    current_literal_clean = current.clean_note

    l = ['do', 're', 'mi', 'fa', 'sol', 'la', 'si']
    found = False
    travessat = False
    counter = 1
    i = l.index(last_literal_clean)
    while not found:
        found = current_literal_clean == l[i]
        if not found:
            counter += 1
            if i+1 == len(l):
                travessat = True
            i = (i+1)%len(l)

    has_modifier = current.modifier_quantity > 0
    modifier = current.modifier
    quantity = current.modifier_quantity

    # basic alture modification: si3 -> do4
    is_asc = counter < 5
    if is_asc:
        if travessat:
            current.set_height(current.height + 1)
    else:
        if not travessat:
            current.set_height(current.height - 1)

    # add , and ' modifiers
    if has_modifier:
        if modifier == "'":
            current.set_height(current.height + quantity)
        else:
            current.set_height(current.height - quantity)

    return current  # modificada


def last_non_rest_note(voice):

    for i in range(len(voice.real_notes) - 1, -1, -1):
        if not voice.real_notes[i].is_rest:
            return voice.real_notes[i]

    return voice.real_notes[len(voice.real_notes) - 1]  # return the last


def parse(filename):
    # TODO borrar comentaris, però no borrar línia plis
    file = open(filename, "r")
    lines = file.readlines()

    chopped_lines = [line.split(' ') for line in lines if line[0] != '#']
    chopped_lines = [elem.strip() for line in chopped_lines for elem in line if elem != '']

    expected = ['number']
    alture = -1
    identifiing_cantus_firmus_voice_number = True

    voices = []
    voice = Voice(8)

    last_note = None

    mark_next_note_as_tied = False

    everything_is_ok = True
    for i, elem in enumerate(chopped_lines):
        if is_what_i_need(elem, expected):
            # state machine
            if elem.isnumeric():
                if identifiing_cantus_firmus_voice_number:
                    cf_voice = int(elem)
                    expected = ['number']
                    identifiing_cantus_firmus_voice_number = False
                else:
                    alture = int(elem)
                    expected = ['{']
            elif elem == '{':
                expected = ['note','}']
            elif it_is_a_note(elem):
                current_note = Note(literal=elem, height=alture, voice_number=len(voices))
                if alture >= 0: # la primera nota sempre serà així
                    last_note = current_note
                    alture = -1
                else:
                    # safety measure:
                    if last_note is None:
                        everything_is_ok = False
                        break
                    # last_note is the same as current_note but modified accordingly the previous note
                    last_note = parse_individual_notes(last_non_rest_note(voice), current_note)

                if mark_next_note_as_tied:
                    if voice.real_notes[len(voice.real_notes) - 1].absolute_note != last_note.absolute_note:
                        everything_is_ok = False
                        print_error_msg(i, voice.real_notes[len(voice.real_notes) - 1], last_note)
                        break
                    last_note.is_tied = True
                    mark_next_note_as_tied = False

                voice.add_note(last_note)
                expected = ['~', 'note','}']
            elif elem == '~':
                mark_next_note_as_tied = True
            elif elem == '}':
                expected = ['number']
                voices.append(voice)
                voice = Voice(8)
                #print()
        else:
            everything_is_ok = False
            print_error_msg(i, expected, elem)
            break

    if everything_is_ok:
        voices[cf_voice].set_voice_to_be_cantus_firmus()
        return voices, cf_voice
    else:
        return [],-1


