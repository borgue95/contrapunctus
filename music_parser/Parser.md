# Parser

## Note input

## Rhythm input

## Voice input

The structure of a voice has this pattern:

    height { note1 [note2 ...] }

where `height` is the relative height of the first note. A `la` at 440Hz
corresponds to a `3`

Voices must be entered ordered: the first voice will be the highest and the
last one, the lowest.