import argparse


class ArgParser:

    def __init__(self):
        parser = argparse.ArgumentParser(description='Optional app description')
        parser.add_argument('-i', '--input', type=str,
                            help='file to be analized')
        self.args = parser.parse_args()

    def get_filename(self):
        return self.args.input