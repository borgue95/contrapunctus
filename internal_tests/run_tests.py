import internal_tests.parser_tests.parser_tests as parser_tests
import internal_tests.intervalic_tests.intervalic_tests as intervalic_tests
import internal_tests.cp_tests.cp_tests as cp_tests
import internal_tests.species_tests.first_specie_tests as first_specie_tests
import internal_tests.species_tests.second_specie_tests as second_specie_tests
import internal_tests.species_tests.third_specie_tests as third_specie_tests
import internal_tests.species_tests.fourth_specie_tests as fourth_specie_tests


def run_tests():
    parser_tests.run_parser_tests()
    intervalic_tests.run_intervalic_tests()
    cp_tests.run_cp_tests()
    first_specie_tests.run_first_specie_tests()
    second_specie_tests.run_first_specie_tests()
    third_specie_tests.run_third_specie_tests()
    fourth_specie_tests.run_forth_specie_tests()


if __name__ == "__main__":
    run_tests()

