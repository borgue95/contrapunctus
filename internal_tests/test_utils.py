from utils import TermColors


def print_failed_test(test_name, message=""):
    print(TermColors.RED + test_name + " FAILED. Reason: " + TermColors.REM + message)


def print_success_test(test_name):
    print(TermColors.GRE + test_name + " PASSED" + TermColors.REM)


def check_equality(test_name, expected, found):
    correct = True
    if len(expected) != len(found):
        print_failed_test(test_name,
                          "Different number of errors found. Expected " +
                          str(len(expected)) + " and found " +
                          str(len(found)))
        correct = False
        print("Expected:")
        for err in expected:
            print(err)
        print("Found:")
        for err in found:
            print(err)
        print()

    for err in zip(expected, found):
        if err[0] != err[1]:
            print_failed_test(test_name,
                              "Errors differ.\n  Expected\n    " +
                              str(err[0]) + "\n  and found\n    " +
                              str(err[1]))
            correct = False

    if correct:
        print_success_test(test_name)
