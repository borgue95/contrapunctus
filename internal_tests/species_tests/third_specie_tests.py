import music_parser.Parser as Parser
from tests.Error import Error
from tests.TestThirdSpecie import TestThirdSpecie
import internal_tests.test_utils as test_utils


test_dir = "test_data/species_tests/s3/test"


def get_data(filename):
    voices, cf_voice = Parser.parse(filename)

    found_errors = []
    for i in range(len(voices)):
        if i != cf_voice:
            test_third_specie = TestThirdSpecie(voices[i], voices[cf_voice])
            test_third_specie.run_tests()

            for error in test_third_specie.errors:
                found_errors.append(error)

    return found_errors


def dissonant_test_01():
    found_errors = get_data(test_dir + "01.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 1", errors, found_errors)


def dissonant_test_02():
    found_errors = get_data(test_dir + "02.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 2", errors, found_errors)


def dissonant_test_03():
    found_errors = get_data(test_dir + "03.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 3", errors, found_errors)


def dissonant_test_04():
    found_errors = get_data(test_dir + "04.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 4", errors, found_errors)


def dissonant_test_05():
    found_errors = get_data(test_dir + "05.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 5", errors, found_errors)


def dissonant_test_06():
    found_errors = get_data(test_dir + "06.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 6", errors, found_errors)


def dissonant_test_07():
    found_errors = get_data(test_dir + "07.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 7", errors, found_errors)


def dissonant_test_08():
    found_errors = get_data(test_dir + "08.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 8", errors, found_errors)


def dissonant_test_09():
    found_errors = get_data(test_dir + "09.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 9", errors, found_errors)


def dissonant_test_10():
    found_errors = get_data(test_dir + "10.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 10", errors, found_errors)


def dissonant_test_11():
    found_errors = get_data(test_dir + "11.con")
    errors = [
        Error(Error.ERROR, "Found eighths in non soft tempos", [0]),
        Error(Error.ERROR, "Found eighths in non soft tempos", [1])
    ]
    test_utils.check_equality("Test third specie intervals 11", errors, found_errors)


def dissonant_test_12():
    found_errors = get_data(test_dir + "12.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 12", errors, found_errors)


def dissonant_test_13():
    found_errors = get_data(test_dir + "13.con")
    errors = [
        Error(Error.ERROR, "Found non second after eighth", [3])
    ]
    test_utils.check_equality("Test third specie intervals 13", errors, found_errors)


def dissonant_test_14():
    found_errors = get_data(test_dir + "14.con")
    errors = []
    test_utils.check_equality("Test third specie intervals 14", errors, found_errors)


def dissonant_test_15():
    found_errors = get_data(test_dir + "15.con")
    errors = [
        Error(Error.ERROR, "Not found consonance after deferred resolution of dissonance", [2, 4]),
        Error(Error.ERROR, "Not found 'cambiata' note in between two eighths", [4])
    ]
    test_utils.check_equality("Test third specie intervals 15", errors, found_errors)


def dissonant_test_16():
    found_errors = get_data(test_dir + "16.con")
    errors = [
        Error(Error.ERROR, "Found invalid number of eights", [2]),
        Error(Error.ERROR, "Found invalid number of eights", [5]),
        Error(Error.ERROR, "Found eighths in non soft tempos", [5])
    ]
    test_utils.check_equality("Test third specie intervals 16", errors, found_errors)


def run_third_specie_tests():
    dissonant_test_01()
    dissonant_test_02()
    dissonant_test_03()
    dissonant_test_04()
    dissonant_test_05()
    dissonant_test_06()
    dissonant_test_07()
    dissonant_test_08()
    dissonant_test_09()
    dissonant_test_10()
    dissonant_test_11()
    dissonant_test_12()
    dissonant_test_13()
    dissonant_test_14()
    dissonant_test_15()
    dissonant_test_16()


