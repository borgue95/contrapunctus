import music_parser.Parser as Parser
from tests.Error import Error
from tests.TestFirstSpecie import TestFirstSpecie
import internal_tests.test_utils as test_utils


test_dir = "test_data/species_tests/s1_test"


def get_data(filename):
    voices, cf_voice = Parser.parse(filename)

    found_errors = []
    for i in range(len(voices)):
        if i != cf_voice:
            test_first_specie = TestFirstSpecie(voices[i], voices[cf_voice])
            test_first_specie.run_tests()

            for error in test_first_specie.errors:
                found_errors.append(error)

    return found_errors


def consonant_test_1():
    found_errors = get_data(test_dir + "01.con")
    errors = []
    test_utils.check_equality("Test first specie intervals 1", errors, found_errors)


def consonant_test_2():
    found_errors = get_data(test_dir + "02.con")
    errors = [
        Error(Error.ERROR, "Beginning of contrapunctus is not perfectly consonant", [0]),
        Error(Error.ERROR, "End of contrapunctus is not perfectly consonant", [16])
    ]
    test_utils.check_equality("Test first specie intervals 2", errors, found_errors)


def consonant_test_3():
    found_errors = get_data(test_dir + "03.con")
    errors = [
        Error(Error.ERROR, "Found dissonant interval in first specie", [8])
    ]
    test_utils.check_equality("Test first specie intervals 3", errors, found_errors)


def consonant_test_4():
    found_errors = get_data(test_dir + "04.con")
    errors = [
        Error(Error.ERROR, "Beginning of contrapunctus must be octave or unison", [0])
    ]
    test_utils.check_equality("Test first specie intervals 4", errors, found_errors)


def consonant_test_5():
    found_errors = get_data(test_dir + "05.con")
    errors = []
    test_utils.check_equality("Test first specie intervals 5", errors, found_errors)


def run_first_specie_tests():
    consonant_test_1()
    consonant_test_2()
    consonant_test_3()
    consonant_test_4()
    consonant_test_5()

