import music_parser.Parser as Parser
import internal_tests.test_utils as test_utils
from tests.TestFourthSpecie import TestFourthSpecie

test_dir = "test_data/species_tests/s4/test"


def get_data(filename):
    voices, cf_voice = Parser.parse(filename)

    found_errors = []
    for i in range(len(voices)):
        if i != cf_voice:
            test_fourth_specie = TestFourthSpecie(voices[i], voices[cf_voice])
            test_fourth_specie.run_tests()

            for error in test_fourth_specie.errors:
                found_errors.append(error)

    return found_errors


def check_special_octaves_1():
    found_errors = get_data(test_dir + "01.con")
    errors = []
    test_utils.check_equality("Test special octaves 1", errors, found_errors)


def check_special_octaves_2():
    found_errors = get_data(test_dir + "02.con")
    errors = []
    test_utils.check_equality("Test special octaves 2", errors, found_errors)


def run_forth_specie_tests():
    check_special_octaves_1()
    check_special_octaves_2()




