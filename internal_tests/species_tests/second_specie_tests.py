import music_parser.Parser as Parser
from tests.Error import Error
from tests.TestSecondSpecie import TestSecondSpecie
import internal_tests.test_utils as test_utils


test_dir = "test_data/species_tests/s2_test"


def get_data(filename):
    voices, cf_voice = Parser.parse(filename)

    found_errors = []
    for i in range(len(voices)):
        if i != cf_voice:
            test_first_specie = TestSecondSpecie(voices[i], voices[cf_voice])
            test_first_specie.run_tests()

            for error in test_first_specie.errors:
                found_errors.append(error)

    return found_errors


def consonant_test_1():
    found_errors = get_data(test_dir + "01.con")
    errors = []
    test_utils.check_equality("Test second specie consonant articulation 1", errors, found_errors)


def consonant_test_2():
    found_errors = get_data(test_dir + "02.con")
    errors = [
        Error(Error.ERROR, "Found dissonant interval when articulating cantus firmus in second specie", [0]),
        Error(Error.ERROR, "Found dissonant interval when articulating cantus firmus in second specie", [8])
    ]
    test_utils.check_equality("Test second specie consonant articulation 1", errors, found_errors)


def consonant_test_3():
    found_errors = get_data(test_dir + "03.con")
    errors = [
        Error(Error.ERROR, "Found non joint degree after dissonant note in second specie", [4, 8])
    ]
    test_utils.check_equality("Test second specie consonant articulation 1", errors, found_errors)


def consonant_test_4():
    found_errors = get_data(test_dir + "04.con")
    errors = [
        Error(Error.ERROR, "Found non joint degree before dissonant note in second specie", [0, 4])
    ]
    test_utils.check_equality("Test second specie consonant articulation 1", errors, found_errors)


def run_first_specie_tests():
    consonant_test_1()
    consonant_test_2()
    consonant_test_3()
    consonant_test_4()

