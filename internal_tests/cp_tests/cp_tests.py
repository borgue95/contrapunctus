import music_parser.Parser as Parser
import utils
import internal_tests.test_utils as tests_utils
from tests.TestCP import TestCP
from tests.Error import Error


def check_equality(test_name, expected, found):
    correct = True
    if len(expected) != len(found):
        tests_utils.print_failed_test(test_name,
                                      "Different number of errors found. Expected " +
                                      str(len(expected)) + " and found " +
                                      str(len(found)))
        correct = False

    for err in zip(expected, found):
        if err[0] != err[1]:
            tests_utils.print_failed_test(test_name,
                                          "Errors differ.\n  Expected\n    " +
                                          str(err[0]) + "\n  and found\n    " +
                                          str(err[1]))
            correct = False

    if correct:
        tests_utils.print_success_test(test_name)


def test_cp01():
    voices, cf_voice = Parser.parse("test_data/cp_tests/cp01.con")
    interval_voices = utils.create_interval_voices(voices)

    test_cp = TestCP(interval_voices[0], voices[cf_voice], len(voices))
    test_cp.run_tests()

    # what it has to be:
    errors = []

    check_equality("Test cp 01", errors, test_cp.errors)


def test_cp02():
    voices, cf_voice = Parser.parse("test_data/cp_tests/cp02.con")
    interval_voices = utils.create_interval_voices(voices)

    test_cp = TestCP(interval_voices[0], voices[cf_voice], len(voices))
    test_cp.run_tests()

    # what it has to be:
    errors = []

    check_equality("Test cp 02", errors, test_cp.errors)


def test_cp03():
    voices, cf_voice = Parser.parse("test_data/cp_tests/cp03.con")
    interval_voices = utils.create_interval_voices(voices)

    test_cp = TestCP(interval_voices[0], voices[cf_voice], len(voices))
    test_cp.run_tests()

    # what it has to be:
    errors = [
        Error(Error.ERROR, "Found CP reached by direct movement", [4])
    ]

    check_equality("Test cp 03", errors, test_cp.errors)


def test_cp04():
    voices, cf_voice = Parser.parse("test_data/cp_tests/cp04.con")
    interval_voices = utils.create_interval_voices(voices)

    errors_found = []
    for interval_voice in interval_voices:
        test_intervals = TestCP(interval_voice, voices[cf_voice], len(voices))
        test_intervals.run_tests()

        for err in test_intervals.errors:
            errors_found.append(err)

    # what it has to be:
    errors = []

    check_equality("Test cp 04", errors, errors_found)


def test_cp05():
    voices, cf_voice = Parser.parse("test_data/cp_tests/cp05.con")
    interval_voices = utils.create_interval_voices(voices)

    errors_found = []
    for interval_voice in interval_voices:
        test_intervals = TestCP(interval_voice, voices[cf_voice], len(voices))
        test_intervals.run_tests()

        for err in test_intervals.errors:
            errors_found.append(err)

    # what it has to be:
    errors = [
        Error(Error.ERROR, "Found CP reached by direct movement", [4])
    ]

    check_equality("Test cp 05", errors, errors_found)


def test_cp06():
    voices, cf_voice = Parser.parse("test_data/cp_tests/cp06.con")
    interval_voices = utils.create_interval_voices(voices)

    errors_found = []
    for interval_voice in interval_voices:
        test_intervals = TestCP(interval_voice, voices[cf_voice], len(voices))
        test_intervals.run_tests()

        for err in test_intervals.errors:
            errors_found.append(err)

    # what it has to be:
    errors = []

    check_equality("Test cp 06", errors, errors_found)


def run_cp_tests():
    test_cp01()
    test_cp02()
    test_cp03()
    test_cp04()
    test_cp05()
    test_cp06()
