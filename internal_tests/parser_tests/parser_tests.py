# TODO english nomenclature

import music_parser.Parser as Parser
import internal_tests.test_utils as test_utils
from music_elements.Note import Note
from music_elements.Voice import Voice


def __prove_equality(voices, expected_voices, test_name):
    success = True
    subtest = 1
    for voice, expected_voice in zip(voices, expected_voices):
        for note, expected_note in zip(voice.notes, expected_voice.notes):
            if note != expected_note:
                success = False
                test_utils.print_failed_test(test_name, "Subtest " + str(subtest) + ". Expected " + str(
                    expected_note) + ". Found " + str(note))
        subtest += 1

    if success:
        test_utils.print_success_test(test_name)


def test_joint_degrees():
    voices, cf_voice = Parser.parse('test_data/parser_tests/joint_degrees.con')

    voice1 = Voice(8)
    voice1.add_note(Note(literal='la4', height=3, duration=4, voice_number=0))
    voice1.add_note(Note(literal='fa', height=3, duration=4, voice_number=0))
    voice1.add_note(Note(literal='do', height=3, duration=4, voice_number=0))
    voice1.add_note(Note(literal='si', height=2, duration=4, voice_number=0))

    voice2 = Voice(8)
    voice2.add_note(Note(literal='mi4', height=3, duration=4, voice_number=1))
    voice2.add_note(Note(literal='re', height=3, duration=4, voice_number=1))
    voice2.add_note(Note(literal='do', height=3, duration=4, voice_number=1))
    voice2.add_note(Note(literal='si', height=2, duration=4, voice_number=1))

    voice3 = Voice(8)
    voice3.add_note(Note(literal='si2', height=3, duration=2, voice_number=2))
    voice3.add_note(Note(literal='do', height=4, duration=2, voice_number=2))
    voice3.add_note(Note(literal='re', height=4, duration=2, voice_number=2))
    voice3.add_note(Note(literal='sol', height=4, duration=2, voice_number=2))
    voice3.add_note(Note(literal='do', height=5, duration=2, voice_number=2))

    voice4 = Voice(8)
    voice4.add_note(Note(literal='sol4', height=3, duration=4, voice_number=3))
    voice4.add_note(Note(literal='do', height=4, duration=4, voice_number=3))
    voice4.add_note(Note(literal='re', height=4, duration=4, voice_number=3))

    voice5 = Voice(8)
    voice5.add_note(Note(literal='do4', height=3, duration=4, voice_number=4))
    voice5.add_note(Note(literal='dob', height=3, duration=4, voice_number=4))
    voice5.add_note(Note(literal='si', height=2, duration=4, voice_number=4))

    voice6 = Voice(8)
    voice6.add_note(Note(literal='si4', height=3, duration=4, voice_number=5))
    voice6.add_note(Note(literal='sid', height=3, duration=4, voice_number=5))
    voice6.add_note(Note(literal='do', height=4, duration=4, voice_number=5))

    expected_voices = [
        voice1,
        voice2,
        voice3,
        voice4,
        voice5,
        voice6
    ]

    __prove_equality(voices, expected_voices, "Joint degrees test")


def test_small_jumps():
    voices, cf_voice = Parser.parse('test_data/parser_tests/small_jumps.con')

    voice1 = Voice(8)
    voice1.add_note(Note(literal='la4', height=3, duration=4, voice_number=0))
    voice1.add_note(Note(literal='mi', height=3, duration=4, voice_number=0))
    voice1.add_note(Note(literal='la', height=3, duration=4, voice_number=0))

    voice2 = Voice(8)
    voice2.add_note(Note(literal='la4', height=3, duration=4, voice_number=1))
    voice2.add_note(Note(literal='mi,', height=2, duration=4, voice_number=1))
    voice2.add_note(Note(literal='la', height=2, duration=4, voice_number=1))

    voice3 = Voice(8)
    voice3.add_note(Note(literal='la4', height=3, duration=4, voice_number=2))
    voice3.add_note(Note(literal='mi\'', height=4, duration=4, voice_number=2))
    voice3.add_note(Note(literal='la', height=4, duration=4, voice_number=2))

    voice4 = Voice(8)
    voice4.add_note(Note(literal='la4', height=3, duration=4, voice_number=3))
    voice4.add_note(Note(literal='mi,', height=2, duration=4, voice_number=3))
    voice4.add_note(Note(literal='la\'', height=3, duration=4, voice_number=3))

    voice5 = Voice(8)
    voice5.add_note(Note(literal='la4', height=3, duration=4, voice_number=4))
    voice5.add_note(Note(literal='mi\'', height=4, duration=4, voice_number=4))
    voice5.add_note(Note(literal='la,', height=3, duration=4, voice_number=4))

    voice6 = Voice(8)
    voice6.add_note(Note(literal='la4', height=3, duration=4, voice_number=5))
    voice6.add_note(Note(literal='re', height=4, duration=4, voice_number=5))
    voice6.add_note(Note(literal='la', height=3, duration=4, voice_number=5))

    voice7 = Voice(8)
    voice7.add_note(Note(literal='la4', height=3, duration=4, voice_number=6))
    voice7.add_note(Note(literal='re,', height=3, duration=4, voice_number=6))
    voice7.add_note(Note(literal='la', height=2, duration=4, voice_number=6))

    voice8 = Voice(8)
    voice8.add_note(Note(literal='la4', height=3, duration=4, voice_number=7))
    voice8.add_note(Note(literal='re\'', height=5, duration=4, voice_number=7))
    voice8.add_note(Note(literal='la', height=4, duration=4, voice_number=7))

    voice9 = Voice(8)
    voice9.add_note(Note(literal='la4', height=3, duration=4, voice_number=8))
    voice9.add_note(Note(literal='re,', height=3, duration=4, voice_number=8))
    voice9.add_note(Note(literal='la\'', height=3, duration=4, voice_number=8))

    voice10 = Voice(8)
    voice10.add_note(Note(literal='la4', height=3, duration=4, voice_number=9))
    voice10.add_note(Note(literal='re\'', height=5, duration=4, voice_number=9))
    voice10.add_note(Note(literal='la,', height=3, duration=4, voice_number=9))

    voice11 = Voice(8)
    voice11.add_note(Note(literal='re4', height=3, duration=4, voice_number=10))
    voice11.add_note(Note(literal='si,', height=1, duration=4, voice_number=10))

    voice12 = Voice(8)
    voice12.add_note(Note(literal='si4', height=3, duration=4, voice_number=11))
    voice12.add_note(Note(literal='sol\'', height=4, duration=4, voice_number=11))

    voice13 = Voice(8)
    voice13.add_note(Note(literal='si4', height=3, duration=4, voice_number=12))
    voice13.add_note(Note(literal='sol,', height=2, duration=4, voice_number=12))

    voice14 = Voice(8)
    voice14.add_note(Note(literal='si4', height=3, duration=4, voice_number=13))
    voice14.add_note(Note(literal='re,,', height=2, duration=4, voice_number=13))

    expected_voices = [
        voice1,
        voice2,
        voice3,
        voice4,
        voice5,
        voice6,
        voice7,
        voice8,
        voice9,
        voice10,
        voice11,
        voice12,
        voice13,
        voice14
    ]

    __prove_equality(voices, expected_voices, "Small jumps test")


def test_rests():
    voices, cf_voice = Parser.parse("test_data/parser_tests/rests.con")

    voice = Voice(8)
    voice.add_note(Note(literal="x4", height=3, duration=4, voice_number=0))
    voice.add_note(Note(literal="re2", height=3, duration=2, voice_number=0))
    voice.add_note(Note(literal="x", height=3, duration=2, voice_number=0))
    voice.add_note(Note(literal="la", height=2, duration=2, voice_number=0))

    __prove_equality([voice], voices, "Rests test")


def ties_tests1():
    voices, cf_voice = Parser.parse("test_data/parser_tests/ties1.con")

    voice = Voice(8)
    voice.add_note(Note(literal="la4", height=3, duration=4, voice_number=0))
    note = Note(literal="la", height=3, duration=4, voice_number=0)
    note.is_tied = True
    voice.add_note(note)

    __prove_equality([voice], voices, "Ties test 1")


def ties_tests2():
    voices, cf_voice = Parser.parse("test_data/parser_tests/ties1.con")

    __prove_equality(voices, [], "Ties test 2")


def run_parser_tests():
    test_joint_degrees()
    test_small_jumps()
    test_rests()
    ties_tests1()
    ties_tests2()
