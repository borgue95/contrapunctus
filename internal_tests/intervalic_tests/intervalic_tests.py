import utils
import music_parser.Parser as Parser
from tests.TestIntervals import TestIntervals
import internal_tests.test_utils as tests_utils
from tests.Error import Error


def check_equality(test_name, expected, found):
    correct = True
    if len(expected) != len(found):
        tests_utils.print_failed_test(test_name,
                                      "Different number of errors found. Expected " +
                                      str(len(expected)) + " and found " +
                                      str(len(found)))
        correct = False

    for err in zip(expected, found):
        if err[0] != err[1]:
            tests_utils.print_failed_test(test_name,
                                          "Errors differ.\n  Expected\n    " +
                                          str(err[0]) + "\n  and found\n    " +
                                          str(err[1]))
            correct = False

    if correct:
        tests_utils.print_success_test(test_name)


def test_octaves():
    voices, cf_voice = Parser.parse("test_data/interval_tests/interval01.con")
    interval_voices = utils.create_interval_voices(voices)

    test_intervals = TestIntervals(interval_voices[0], voices[cf_voice])
    test_intervals.run_tests()

    # what it has to be:
    errors = [Error(Error.ERROR, "Found octaves", [0, 8])]

    check_equality("Test octaves", errors, test_intervals.errors)


def test_deferred_octaves1():
    voices, cf_voice = Parser.parse("test_data/interval_tests/interval02.con")
    interval_voices = utils.create_interval_voices(voices)

    test_intervals = TestIntervals(interval_voices[0], voices[cf_voice])
    test_intervals.run_tests()

    # what it has to be:
    errors = [Error(Error.ERROR, "Found deferred octaves", [0, 8])]

    check_equality("Test deferred octaves 1", errors, test_intervals.errors)


def test_deferred_octaves2():
    voices, cf_voice = Parser.parse("test_data/interval_tests/interval03.con")
    interval_voices = utils.create_interval_voices(voices)

    test_intervals = TestIntervals(interval_voices[0], voices[cf_voice])
    test_intervals.run_tests()

    # what it has to be:
    errors = []

    check_equality("Test deferred octaves 2", errors, test_intervals.errors)


def test_deferred_fifths1():
    voices, cf_voice = Parser.parse("test_data/interval_tests/interval04.con")
    interval_voices = utils.create_interval_voices(voices)

    test_intervals = TestIntervals(interval_voices[0], voices[cf_voice])
    test_intervals.run_tests()

    # what it has to be:
    errors = [Error(Error.ERROR, "Found deferred fifths", [4, 10])]

    check_equality("Test deferred fifths 1", errors, test_intervals.errors)


def test_deferred_fifths2():
    voices, cf_voice = Parser.parse("test_data/interval_tests/interval05.con")
    interval_voices = utils.create_interval_voices(voices)

    errors_found = []
    for interval_voice in interval_voices:
        test_intervals = TestIntervals(interval_voice, voices[cf_voice])
        test_intervals.run_tests()

        for err in test_intervals.errors:
            errors_found.append(err)

    # what it has to be:
    errors = [
        # Error(Error.WARNING, "Found deferred fifths, but are dissonant with cantus firmus", [2, 8])
    ]

    check_equality("Test deferred fifths 2", errors, errors_found)


def test_deferred_fifths3():
    voices, cf_voice = Parser.parse("test_data/interval_tests/interval06.con")
    interval_voices = utils.create_interval_voices(voices)

    errors_found = []
    for interval_voice in interval_voices:
        test_intervals = TestIntervals(interval_voice, voices[cf_voice])
        test_intervals.run_tests()

        for err in test_intervals.errors:
            errors_found.append(err)

    # what it has to be:
    errors = [
        # Error(Error.WARNING, "Found deferred fifths, but are dissonant with cantus firmus", [2, 10]),
        # Error(Error.WARNING, "Found deferred fifths, but are dissonant with cantus firmus", [10, 16])
    ]

    check_equality("Test deferred fifths 3", errors, errors_found)


def test_deferred_fifths4():
    voices, cf_voice = Parser.parse("test_data/interval_tests/interval07.con")
    interval_voices = utils.create_interval_voices(voices)

    test_intervals = TestIntervals(interval_voices[0], voices[cf_voice])
    test_intervals.run_tests()

    # what it has to be:
    errors = []

    check_equality("Test deferred fifths 4", errors, test_intervals.errors)


def test_deferred_fifths5():
    voices, cf_voice = Parser.parse("test_data/interval_tests/interval08.con")
    interval_voices = utils.create_interval_voices(voices)

    test_intervals = TestIntervals(interval_voices[0], voices[cf_voice])
    test_intervals.run_tests()

    # what it has to be:
    errors = [Error(Error.ERROR, "Found deferred fifths", [4, 12])]

    check_equality("Test deferred fifths 5", errors, test_intervals.errors)


def test_thirds():
    voices, cf_voice = Parser.parse("test_data/interval_tests/interval09.con")
    interval_voices = utils.create_interval_voices(voices)

    test_intervals = TestIntervals(interval_voices[0], voices[cf_voice])
    test_intervals.run_tests()

    # what it has to be:
    errors = [Error(Error.ERROR, "Found more than 3 thirds in a row", [12])]

    check_equality("Test thirds", errors, test_intervals.errors)


def test_sixths():
    voices, cf_voice = Parser.parse("test_data/interval_tests/interval10.con")
    interval_voices = utils.create_interval_voices(voices)

    test_intervals = TestIntervals(interval_voices[0], voices[cf_voice])
    test_intervals.run_tests()

    # what it has to be:
    errors = [Error(Error.ERROR, "Found more than 3 sixths in a row", [12])]

    check_equality("Test sixths", errors, test_intervals.errors)


def run_intervalic_tests():
    test_octaves()
    test_deferred_octaves1()
    test_deferred_octaves2()
    test_deferred_fifths1()
    test_deferred_fifths2()
    test_deferred_fifths3()
    test_deferred_fifths4()
    test_deferred_fifths5()
    test_thirds()
    test_sixths()
