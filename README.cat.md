# Contrapunctus

Llegiu-me en [anglès](README.md)

## Contacte

Podeu obrir _issues_ aquí mateix si sabeu com funciona; si no, podeu escriure'm un correu electrònic a 
`berenguer.95@gmail.com`. 

## Requisits

És necessari tenir instal·lat el llenguatge de programació `Python 3`.

* **Windows**

    Es pot obtenir gratuïtament des de la 
    [pàgina oficial](https://www.python.org/ftp/python/3.7.2/python-3.7.2-amd64.exe).

* **MacOS**

    Segurament ja està instal·lat. Si no, el podeu obtenir des de la
    [pàgina oficial](https://www.python.org/ftp/python/3.7.2/python-3.7.2-macosx10.9.pkg).
    
* **Linux**

    Molt segurament ja està instal·lat. Proveu-ho obrint un terminal i entrant aquesta comanda:
    
        which python3
    
    Si retorna quelcom com `/usr/bin/python3`, el teniu instal·lat. En cas contrari, el podeu instal·lar amb el gestor
    de paquests que preferiu. Algun exemple:
    
    * Debian/Ubuntu: `sudo apt-get install python3`
    * Arch Linux: `sudo pacman -S python3`
    * Fedora: `sudo dnf -i python3`


## Ús

Aquesta primera versió de Contrapunctus no disposa d'interfície gràfica, per tant, s'ha d'executar per terminal, però
prometo que és molt senzill. 

Descarregeu-vos el zip i descomprimiu-lo. 

* **Windows**

    Pendent de provar-ho.

* **MacOS** i **Linux**

    * Obriu un terminal i navegeu cap a la carpeta descomprimida (`cd`)
    * Entreu aquesta comanda: 
            
            python3 contrapunctus.py -i el_vostre_fitxer.con
        
        on `el_vostre_fitxer.con` és el contrapunt que voleu analitzar. 
        A la secció [Sintaxi](#Sintaxi) explico com s'ha de crear aquest fitxer. 
        Entreu la ruta sencera del fitxer (o bé arrastreu-lo al terminal). 

De seguida, el programa mostrarà, pel terminal mateix, els errors que hagi pogut trobar. Si trobeu falsos positius o
falsos negatius, feu-m'ho saber. 

Aquest programa encara no és prou madur per ser a prova de bombes. És possible que pugui petar inesperadament. En aquest
cas, feu-m'ho saber, adjuntant el fitxer d'entrada. 


__*Atenció*__: no he provat d'executar aquest programa ni en Windows ni en MacOS. El programa en sí hauria de funcionar
sense problemes; el que no pot funcionar és la manera d'executar-lo tal com ho explicat aquí. 



## Sintaxi

El programa no és capaç (ni pot ser-ho) de llegir ni partitures manuscrites ni partitures escrites amb programes
populars com Finale o Sibelius. Per aquest motiu, he creat una sintaxi per escriure partitures molt senzilles perquè
el programa sigui capaç d'interpretar-les. 

Els fitxers tenen una extensió (opcional, però recomanada) `.con`, que és l'abreviatura del nom del programa, 
Contrapunctus. Aquests fitxers són de text pla, és a dir, text sense format. Podeu crear aquests fitxers des del 
"Bloc de notes" de Windows o des de "Text edit" a MacOS. 

L'estructura del fitxer és la següent:

* Posició del cantus firmus en relació a la veu més aguda. Comença a comptar per 0. És un número
* Les diferents veus, ordenades de més aguda a més greu.

Cada veu té aquest format:

* Alçada de la primera nota que no sigui silenci. Per exemple, si la primera nota és un la4 (440Hz), aquest camp seria
`4`.
* `{`
* Les diferents notes en notació catalana (`do` `re` `mi` `fa` `sol` `la` `si`) amb modificadors per bemoll (`b`) i 
    diesi (`d`). Per exemple, la bemooll s'escriu `lab`, sense espai. Els silencis s'escriuen amb una `x`.
    
    * L'escriptura és relativa a la primera nota, per tant, si la distància entre notes és més petita d'una quinta, no
        cal afegir cap modificador extra. Per exemple, una escala ascendent que comença per la i acaba en mi, 
        s'escriuria així: `la si do re mi`
    * Si es necessita fer un salt de quinta o més, cal afegir el modificador cometa (`'`) per un salt ascendent o bé
        el modificador coma (`,`) per un salt descdendent. Per exemple, un salt de quinta ascendent la - mi s'escriuria
        així: `la mi'` i un salt de sisena descendent la do, s'escriuria així: `la do,`. 
    * El **ritme**. Utilitza nombres habituals: 1 és una rodona, 2, una blanca, 4 una negra i 8 una corxera. El nombre 
        que identifica el ritme va enganxat a la nota: una negra la bemoll, s'escriu `lab4`. Per haver d'evitar escriure
        el ritme quan totes les notes tenen el mateix, només cal indicar aquest nombre a la primera nota i quan es
        desitgi canviar de ritme.
* `}`

És molt important que entre cadascun els elements hi hagui com a mínim un espai, és a dir: entre l'alçada de la primera
nota i la clau, entre la clau i la primera nota, entre les notes i entre la darrera nota i la clau. Hi ha
[exemples](#Exemples)
més avall. 

### Exemples

Aquest contrapunt

![Exemple 1](docs/examples/example01.png)

s'escriuria així:

    1
    3 { x4 sol fa mi fad2 la re,4 do mib fa }
    2 { do1 re sol }

## Regles

Pendent d'escriure-les.