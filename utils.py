import itertools
from music_elements.Interval import Interval


class TermColors:
    GRA = '\033[90m'
    RED = '\033[91m'
    GRE = '\033[92m'
    YEL = '\033[93m'
    BLU = '\033[94m'
    MAG = '\033[95m'
    CYA = '\033[96m'

    REM = '\033[0m'
    BOL = '\033[1m'
    UND = '\033[4m'


def create_interval_voices(voices):
    group_of_voices = list(itertools.combinations(range(len(voices)), 2))

    intervals_voices = []
    for group in group_of_voices:
        voice1 = voices[group[0]]
        voice2 = voices[group[1]]

        assert len(voice1.notes) == len(voice2.notes)

        intervals_voice = []
        for tempo_mark, two_notes in enumerate(zip(voice1.notes, voice2.notes)):
            interval = Interval(two_notes, tempo_mark)
            if not interval.redundant:
                intervals_voice.append(interval)

        for i in range(len(intervals_voice)-1):
            intervals_voice[i].last_tempo_mark = intervals_voice[i+1].tempo_mark - 1

        intervals_voices.append(intervals_voice)

    return intervals_voices

