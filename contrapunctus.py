import internal_tests.cp_tests.cp_tests as cp_tests
import internal_tests.intervalic_tests.intervalic_tests as intervalic_tests
import internal_tests.species_tests.first_specie_tests as first_specie_tests
import internal_tests.species_tests.fourth_specie_tests as fourth_specie_tests
import internal_tests.species_tests.second_specie_tests as second_specie_tests
import internal_tests.species_tests.third_specie_tests as third_specie_tests

from arg_parser import ArgParser
import utils
import music_parser.Parser as Parser


def main():

    args = ArgParser()

    voices, cf_voice = Parser.parse(args.get_filename())
    interval_voices = utils.create_interval_voices(voices)

    for aaa in interval_voices:
        test_cp = cp_tests.TestCP(aaa, voices[cf_voice], len(voices))
        test_in = intervalic_tests.TestIntervals(aaa, voices[cf_voice])

        test_cp.run_tests()
        test_in.run_tests()

        test_cp.print_errors()
        test_in.print_errors()

    for i in range(len(voices)):
        if i != cf_voice:
            test_1_specie = first_specie_tests.TestFirstSpecie(voices[i], voices[cf_voice])
            test_2_specie = second_specie_tests.TestSecondSpecie(voices[i], voices[cf_voice])
            test_3_specie = third_specie_tests.TestThirdSpecie(voices[i], voices[cf_voice])
            test_4_specie = fourth_specie_tests.TestFourthSpecie(voices[i], voices[cf_voice])

            test_1_specie.run_tests()
            test_2_specie.run_tests()
            test_3_specie.run_tests()
            test_4_specie.run_tests()

            test_1_specie.print_errors()
            test_2_specie.print_errors()
            test_3_specie.print_errors()
            test_4_specie.print_errors()


if __name__ == "__main__":
    main()
